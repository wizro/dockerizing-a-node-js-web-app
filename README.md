## Dockerizing a Node-js Web App

A simple example on how to dockerize a simple nodejs  web app and run it from container.
The inspiration for this project guide was taken from [node js](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/) website visit the link for more information.

## About App

This app doesn't really do any thing interesting rather than to print hello world on the web browser, the purpose of this project is to demonstrate how to get a Node.js application into a Docker container. As such the focuse is not on the application rather on the process of getting the application into a a Docker container. 

## Installation

`git clone https://gitlab.com/wizro/dockerizing-a-node-js-web-app.git`

clone this repository

`cd dockerizing-a-node-js-web-app`

move into a project root directory

`docker Dockerfile build -t test/web-app .`

build docker image

`docker run -p 49181:8080 -d test/web-app`

run containerized app on http://localhost:49181

## Test

visit http://localhost:49181 on your browser and `Hello World` text should be dislayed on page
